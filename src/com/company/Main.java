package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        char grade;

        Scanner readGrade= new Scanner(System.in);

        System.out.print("Enter an grade: ");
        grade = readGrade.next().charAt(0);

        String message;
        switch(grade){
            case 'A' : message = ("Excellent!"); break;
            case 'B' : message = ("Great!"); break;
            case 'C' : message = ("Well done"); break;
            case 'D' : message = ("You passed");break;
            default : message = ("Invalid grade"); break;
        }
        System.out.println(message);

    }
}
